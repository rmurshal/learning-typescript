function checkSpeedOfAnimal(animal) {
    var speed = 0;
    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed;
            break;
        case 'snake':
            speed = animal.crawlingSpeed;
            break;
        case 'horse':
            speed = animal.runningSpeed;
            break;
        default:
            break;
    }
    console.log("Speed of the animal is ".concat(speed));
}
var snake = {
    type: 'snake',
    crawlingSpeed: 20
};
var bird = {
    type: 'bird',
    flyingSpeed: 30
};
checkSpeedOfAnimal(snake);
checkSpeedOfAnimal(bird);
