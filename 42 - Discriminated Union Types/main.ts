/**
 * Cek apakah object mengandung property a or b or c
 * Beda dengan yang di Type Guard Checking, mengecek apakah object mengandung sebuah property saja
 */

interface Bird {
    type: 'bird'
    flyingSpeed: number
}

interface Snake {
    type: 'snake'
    crawlingSpeed: number
}

interface Horse {
    type: 'horse'
    runningSpeed: number
}

type Animal = Bird | Snake | Horse

function checkSpeedOfAnimal(animal: Animal) {
    let speed = 0
    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed
            break

        case 'snake':
            speed = animal.crawlingSpeed
            break

        case 'horse':
            speed = animal.runningSpeed
            break

        default:
            break
    }
    console.log(`Speed of the animal is ${speed}`)
}

const snake: Snake = {
    type: 'snake',
    crawlingSpeed: 20,
}

const bird: Bird = {
    type: 'bird',
    flyingSpeed: 30,
}

checkSpeedOfAnimal(snake)
checkSpeedOfAnimal(bird)
