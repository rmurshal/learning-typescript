# Discriminated Union Types

Bagian ini menjelaskan bagaimana menentukan apakah sebuah object mengandung property A, atau B, atau C. Berbeda dengan yang sebelumnya yang hanya mengecek apakah object mengandung property tertentu menggunakan `property in object` atau `object instanceof class`.

_Notes: Apabila kita mengecek menggunakan 2 cara di atas, ada kemungkinan terjadi kesalahan apabila kita terdapat type._

```ts
interface Bird {
    type: 'bird'
    flyingSpeed: number
}

interface Snake {
    type: 'snake'
    crawlingSpeed: number
}

interface Horse {
    type: 'horse'
    runningSpeed: number
}

type Animal = Bird | Snake | Horse

function checkSpeedOfAnimal(animal: Animal) {
    let speed = 0
    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed
            break

        case 'snake':
            speed = animal.crawlingSpeed
            break

        case 'horse':
            speed = animal.runningSpeed
            break

        default:
            break
    }
    console.log(`Speed of the animal is ${speed}`)
}

const snake: Snake = {
    type: 'snake',
    crawlingSpeed: 20,
}

const bird: Bird = {
    type: 'bird',
    flyingSpeed: 30,
}

checkSpeedOfAnimal(snake)
checkSpeedOfAnimal(bird)
```
