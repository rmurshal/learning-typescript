# outFile

`outFile` adalah property yang mengizinkan kita untuk menggabungkan (concat) seluruh file typescript yang ada menjadi satu buah file javascript. Apabila `declaration` true, maka akan menghasilkan satu buah file `.d.ts` juga yang telah menggabungkan semuanya.

# outDir

Property yang mengizinkan untuk menspesifikkan lokasi hasil kompilasi file typescript yang ada. Contoh:

```json
"outDir": "./dist" // Simpan hasil kompilasi dalam folder dist
```

# rootDir

Property yang berguna sebagai input path file typescript yang akan kita kompilasi.

```json
"rootDir": "./js" // Kompilasi semua file typescript yang ada pada folder js
```

# removeComments

Menghapus semua komentar yang ada pada file typescript.

# noEmit

Property yang membuat typescript hanya melakukan kompilasi file typescript tanpa menghasilkan output file javascript.

# downlevelIteration

Property yang akan membuat beberapa fungsi modern pada ES6 seperti `for / of loop`, `spread array`, `spread argument`, dan `Symbol.iterator` dapat dikompilasi dengan baik oleh kompiler. Downgrade dari hal ini hanya akan menambahkan kode tambahan pada file javascript yang akan dihasilkan yang tentunya akan berpengaruh kepada performa dari aplikasi.

_Source: https://www.typescriptlang.org/tsconfig#downlevelIteration_
