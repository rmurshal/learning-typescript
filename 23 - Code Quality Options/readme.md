# Beberapa property yang disini berguna untuk code quality check

# noUnusedLocal

Property yang berguna untuk memberi error apabila terdapat variabel local yang dideklarasikan tetapi tidak digunakan.

# noUnusedParameters

Property yang berguna untuk memberi error apabila terdapat parameter function yang tidak digunakan.

# noImplicitReturns

Property yang berguna untuk memberi error apabila terdapat ke-tidak-konsistenan sebuah function untuk mereturn sebuah value (ada jalur pada perkondisian yang tidak mereturn sebuah value).
