var Department = /** @class */ (function () {
    function Department(id, name) {
        this.id = id;
        this.name = name;
        this.employees = [];
    }
    Department.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    // static
    Department.returnEmployee = function (employee) {
        return { employee: employee };
    };
    Department.prototype.printEmployees = function () {
        console.log(this.employees, this.employees.length);
    };
    Department.prototype.describe = function () {
        console.log("Department with id ".concat(this.id, " : ").concat(this.name));
        // trying access static property here
        console.log(Department.currentYear);
    };
    // static
    Department.currentYear = new Date().getFullYear();
    return Department;
}());
// accessing static property or method
console.log(Department.currentYear);
console.log(Department.returnEmployee('Sutanto'));
var DTI = new Department('DTI', 'Information Technology');
DTI.describe();
