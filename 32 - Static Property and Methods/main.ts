class Department {
    protected employees: string[] = []
    // static
    static currentYear = new Date().getFullYear()

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    // static
    static returnEmployee(employee: string) {
        return { employee }
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)

        // trying access static property here
        console.log(Department.currentYear)
    }
}

// accessing static property or method
console.log(Department.currentYear)
console.log(Department.returnEmployee('Sutanto'))

const DTI = new Department('DTI', 'Information Technology')
DTI.describe()
