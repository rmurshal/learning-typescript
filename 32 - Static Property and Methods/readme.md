# Static Property and Method

Untuk membuat property atau method static pada sebuah class, kita dapat menggunakna keyword `static`. Property atau method `static` memungkinkan kita untuk mengakses property atau method pada sebuah class tanpa perlu membuat instancenya terlebih dahulu. Berikut contoh:

```ts
class Department {
    protected employees: string[] = []
    // static
    static currentYear = new Date().getFullYear()

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    // static
    static returnEmployee(employee: string) {
        return { employee }
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)

        // trying access static property here
        console.log(Department.currentYear)
    }
}

// accessing static property or method
console.log(Department.currentYear)
console.log(Department.returnEmployee('Sutanto'))

const DTI = new Department('DTI', 'Information Technology')
DTI.describe()
```

Notes:

-   Kita tidak dapat mengakses property dan method yang diset sebagai static di dalam method normal class menggunakan this. Begitupun sebaliknya, kita tidak dapat mengakses property dan method normal pada class di dalam method static. Hal ini karena property atau method yang diset sebagai static bukan merupakan bagian dari class.
-   Tetapi kita dapat mengakses property dan method yang diset sebagai static di dalam method normal class menggunakan `NamaClass.PropORMethod`
