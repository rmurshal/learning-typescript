# Property Validation Decorator

Berikut adalah contoh mengembangkan sebuah validasi property sederhana dengan menggunakan property decorator:

### HTML

```html
<form>
    <input type="text" id="title" placeholder="Course Title" />
    <input type="text" id="price" placeholder="Course Price" />
    <button type="submit">Submit</button>
</form>
```

### Typescript

```ts
// Menyiapkan interface untuk menampung nilai validasi property pada sebuah class
interface IValidationConfig {
    [property: string]: {
        [validationProperty: string]: string[]
    }

    // we want to build something like this
    // Course: {
    //     title: ['required']
    //     price: ['required']
    // }
}

// Variabel yang menerapkan interface IValidationConfig
const validatorObject: IValidationConfig = {}

// Decorator
function Required(target: any, name: string) {
    const className = target.constructor.name
    validatorObject[className] = {
        ...validatorObject[className],
        [name]: ['required'],
    }
}

// Decorator
function Positive(target: any, name: string) {
    const className = target.constructor.name
    validatorObject[className] = {
        ...validatorObject[className],
        [name]: ['positive'],
    }
}

// Fungsi validasi
function validate(obj: any) {
    let validatorName = validatorObject[obj.constructor.name]

    if (!validatorName) {
        return true
    }

    let isValid = true

    for (const prop in validatorName) {
        for (const validator of validatorName[prop]) {
            switch (validator) {
                case 'required':
                    isValid = isValid && !!obj[prop]
                    break

                case 'positive':
                    isValid = obj[prop] > 0
                    break
            }
        }
    }

    return isValid
}

class Course {
    // Apply decorator here...

    @Required
    title: string
    @Positive
    price: number

    constructor(title: string, price: number) {
        this.title = title
        this.price = price
    }
}

const form = document.querySelector('form')!

form.addEventListener('submit', (event) => {
    event.preventDefault()

    const titleEl = document.getElementById('title') as HTMLInputElement
    const priceEl = document.getElementById('price') as HTMLInputElement

    const title = titleEl.value
    const price = +priceEl.value

    const myCourse = new Course(title, price)

    // Run validation
    if (!validate(myCourse)) {
        alert('Input values are not valid')
        return
    }

    console.log(myCourse)
})
```
