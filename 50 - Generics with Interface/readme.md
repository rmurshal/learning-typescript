# Generics with Interface

Berikut adalah contoh menggunakan generics dengan bantuan interface untuk memastikan sebuah variabel memiliki property tertentu:

```ts
interface Lengthy {
    length: number
}

// All T must have .length property
function getCountAndDescribe<T extends Lengthy>(element: T) {
    let text = 'Got No Element'

    if (element.length === 1) {
        text = 'Got 1 Element'
    }

    if (element.length > 1) {
        text = `Got ${element.length} Elements`
    }

    return [element, text]
}

console.log(getCountAndDescribe('Hi John')) // Ok
console.log(getCountAndDescribe(['Kluster', 'IoT'])) // Ok
console.log(getCountAndDescribe(999) // Error
```

## Bonus

Berikut adalah contoh untuk menambahkan return type pada function menggunakan generics:

```ts
function getCountAndDescribe<T extends Lengthy>(element: T): [T, string] {}
```

Sehingga, fungsi `getCountAndDescribe()` harus mereturn sebuah `tuple` yang berisi tipe data tersebut.
