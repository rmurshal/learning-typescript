function getCountAndDescribe(element) {
    var text = 'Got No Element';
    if (element.length === 1) {
        text = 'Got 1 Element';
    }
    if (element.length > 1) {
        text = "Got ".concat(element.length, " Elements");
    }
    return [element, text];
}
console.log(getCountAndDescribe('Hi John'));
