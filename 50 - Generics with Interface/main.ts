interface Lengthy {
    length: number
}

function getCountAndDescribe<T extends Lengthy>(element: T) {
    let text = 'Got No Element'

    if (element.length === 1) {
        text = 'Got 1 Element'
    }

    if (element.length > 1) {
        text = `Got ${element.length} Elements`
    }

    return [element, text]
}

console.log(getCountAndDescribe('Hi John'))
