var App;
(function (App) {
    var Post = /** @class */ (function () {
        function Post() {
            this.title = 'Some Title';
            this.description = 'Some Description';
        }
        return Post;
    }());
    App.Post = Post;
})(App || (App = {}));
