# Namespace

`Namespace` digunakan untuk mengelompokkan fungsi, kelas, atau interface pada satu ruang lingkup yang sama yang berguna untuk mencegah terjadinya konflik penamaan antara hal tersebut dalam sebuah aplikasi TypeScript.

Berikut adalah contoh basic dari `namespace`:

```ts
namespace App {
    export class MyClass {
        // ...
    }

    export interface MyInterface {
        // ...
    }

    export function myFunction() {
        // ...
    }
}

const myObj = new App.MyClass()
const myVar: App.MyInterface = {
    /* ... */
}
App.myFunction()
```

Apabila namespace akan digunakan pada file lain, kita harus menggunakan `<reference path="">`, berikut contohnya:

File `main.ts`

```ts
// Harus mengambil referensi dari ipost untuk dapat menggunakan IPost
///<reference path="ipost.ts" />

namespace App {
    class Post implements IPost {
        title: string
        description: string

        constructor() {
            this.title = 'Some Title'
            this.description = 'Some Description'
        }
    }

    const myPost = new Post()
    console.log(myPost)
}
```

File `ipost.ts`:

```ts
namespace App {
    export interface IPost {
        title: string
        description: string
    }
}
```

Akan tetapi apabila struktur file kita seperti ini:

File `main.ts`

```ts
// Harus mengambil referensi dari ipost untuk dapat menggunakan IPost
///<reference path="ipost.ts" />

namespace App {
    const myPost = new Post()
    console.log(myPost)
}
```

File `ipost.ts`:

```ts
namespace App {
    export interface IPost {
        title: string
        description: string
    }

    export class Post implements IPost {
        title: string
        description: string

        constructor() {
            this.title = 'Some Title'
            this.description = 'Some Description'
        }
    }
}
```

Maka kita harus melakukan:

-   Bundle kedalam satu file yang sama menggunakan konfigurasi `outFile` pada `tsconfig`
-   Menggunakan module `amd` pada `tsconfig`

Hal ini harus dilakukan agar JavaScript mengetahui adanya `class Post` saat ia dipanggil.
