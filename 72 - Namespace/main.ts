// Harus mengambil referensi dari ipost untuk dapat menggunakan IPost
///<reference path="ipost.ts" />

namespace App {
    class Post implements IPost {
        title: string
        description: string

        constructor() {
            this.title = 'Some Title'
            this.description = 'Some Description'
        }
    }

    const myPost = new Post()
    console.log(myPost)
}
