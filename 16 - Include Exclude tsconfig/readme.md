# Include

Property `include` yang ada pada `tsconfig.json` (sejajar dengan property `compilerOptions`) berguna sebagai wildcard untuk file atau folder yang akan di kompilasi oleh compiler typescript.

```json
// Include 1 file
"include": ["js/main.ts"]

// Include seluruh file dalam 2 level
"include": ["**/*"]

// Include seluruh file dalam folder js
"include": ["js/*"]
```

# Exclude

Property `exclude` yang ada pada `tsconfig.json` (sejajar dengan property `compilerOptions`) berguna sebagai wildcard untuk file atau folder yang tidak akan dimasukkan dalam kompilasi oleh compiler typescript.

```json
// Exclude node_modules big folder
"exclude": ["node_modules"]

// Exclude 1 file
"exclude": ["js/analytics.ts"]
```

# Files

Berfungsi sama dengan `include`, tetapi tidak sebagai wildcard dan hanya spesifik ke sebuah file. Biasanya dipakai untuk mini project.

```json
"files": ["js/main.ts"]
```
