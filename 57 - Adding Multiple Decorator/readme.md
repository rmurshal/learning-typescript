# Multiple Decorator

Kita juga dapat menggunakan beberapa decorator pada sebuah class. Berikut adalah contohnya:

```ts
function Logger(logString: string) {
    console.log('Logger factory')
    return function (constructor: Function) {
        console.log(logString)
        console.log(constructor)
    }
}

function WithTemplate(template: string, hookId: string) {
    console.log('WithTemplate factory')
    return function (constructor: any) {
        console.log('Hello from WithTemplate decorator')
        const hookEl = document.getElementById(hookId)
        const data = new constructor()
        if (hookEl) {
            hookEl.innerHTML = template
            setTimeout(() => {
                const h1 = hookEl.querySelector('h1')!
                h1.innerText = data.name
            }, 3000)
        }
    }
}

@Logger('Hello from Logger decorator')
@WithTemplate('<h1>Hello, World!</h1>', 'app')
class Person {
    name = 'Blz'
    constructor() {
        console.log('Creating object...')
    }
}
```

Selanjutnya, berikut adalah urutan eksekusi program untuk sebuah decorator:

-   Logger factory
-   WithTemplate factory
-   Hello from WithTemplate decorator
-   Creating object...
-   Hello from Logger decorator
-   Person class

Berdasarkan urutan tersebut, kita dapat menyimpulkan:

-   Untuk decorator factory, urutan eksekusi dari top ke bottom
-   Untuk decorator main, urutan eksekusi dari bottom ke top
