# allowJs

Default value `true`, yang berarti seluruh file javascript yang ada juga akan di kompilasi oleh typescript.

```js
// @filename: card.js
export const defaultCardDeck = 'Heart'
```

`allowJs false`:

```ts
// @filename: index.ts
// error
import { defaultCardDeck } from './card'
console.log(defaultCardDeck)
```

`allowJs true`:

```ts
// @filename: index.ts
// ok
import { defaultCardDeck } from './card'
console.log(defaultCardDeck)
```

# checkJs

Bekerja bersama `allowJs`, akan memeriksa apakah ada error dalam file javascript yang akan di kompilasi.

### Example of not getting any error

```js
// @filename: constants.js
module.exports.pi = parseFloat(3.124)
```

```ts
// @filename: index.ts
import { pi } from './constants'
console.log(pi)
```

### Example of getting error

```js
// @filename: constants.js
// Error: Argument of type 'number' is not assignable to parameter of type 'string'.
module.exports.pi = parseFloat(3.124)
```

```ts
// @filename: index.ts
import { pi } from './constants'
console.log(pi)
```

# jsx

Mengizinkan kita agar dapat menggunakan `.tsx` file dalam project. Terdapat 3 value:

-   `preserve`: mempertahankan ekstensi `.jsx` yang selanjutnya dapat diproses oleh alat transformasi (contoh babel).
-   `react`: memanggil `React.createElement()` dan tidak perlu melalui transformasi `.jsx` dan outputnya berupa file `.js`.
-   `react-native`: sama seperti `preserve`, tetapi outpunya akan berupa file `.js`.

# declaration

Default value `true` dan akan menghasilkan file `.d.ts` yang berguna sebagai manifest saat mengembangkan sebuah library yang ditulis menggunakan kode typescript.

# declarationMap

Default value `true` dan akan menghasilkan sourcemap untuk file `.d.ts` di atas.

_Notes: `declaration` dan `declarationMap` is advance topics._

# sourceMap

Akan menghasilkan file `.map` yang akan berguna untuk melakukan proses debugging pada browser. Secara default, sistem hanya akan mengirim file `.js` yang sudah dikompilasi sebelumnya, yang mana kode di dalam file tersebut pastinya akan semakin susah dibaca sejalan dengan banyaknya logika yang digunakan. Sehingga akan lebih mudah jika kita terdapat file `.ts` sebelumnya juga. Untuk mengizinkan hal itu dapat terjadi, kita dapat menggunakan property `sourceMap` ini.

### Example **without** sourceMap

![Project without source map](./1-without-soruce-map.png)

### Example **with** sourceMap

![Project with source map](./2-with-source-map.png)
