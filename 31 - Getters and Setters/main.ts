class Department {
    protected employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'Information Technology')
    }

    addEmployee(employee: string): void {
        if (employee === 'Pak Hari') {
            return
        }
        this.employees.push(employee)
    }

    // implementing getters (it's property)
    get recentAdmin() {
        if (!this.admins) {
            throw new Error('No Admin')
        }
        return this.admins[this.admins.length - 1]
    }

    // implementing setters (it's method)
    set recentAdmin(admin: string) {
        if (!admin) {
            throw new Error('Admin cannot be inserted')
        }
        this.admins.push(admin)
    }
}

const DTI1 = new ITDepartment('DTI1', ['Pak Agus', 'Pak Hari'])
DTI1.recentAdmin = 'Mas Dimas'
console.log(DTI1.recentAdmin)
