class Department {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.employees = [];
    }
    addEmployee(employee) {
        this.employees.push(employee);
    }
    printEmployees() {
        console.log(this.employees, this.employees.length);
    }
    describe() {
        console.log(`Department with id ${this.id} : ${this.name}`);
    }
}
class ITDepartment extends Department {
    constructor(id, admins) {
        super(id, 'Information Technology');
        this.admins = admins;
    }
    addEmployee(employee) {
        if (employee === 'Pak Hari') {
            return;
        }
        this.employees.push(employee);
    }
    // implementing getters (it's property)
    get recentAdmin() {
        if (!this.admins) {
            throw new Error('No Admin');
        }
        return this.admins[this.admins.length - 1];
    }
    // implementing setters (it's method)
    set recentAdmin(admin) {
        if (!admin) {
            throw new Error('Admin cannot be inserted');
        }
        this.admins.push(admin);
    }
}
const DTI1 = new ITDepartment('DTI1', ['Pak Agus', 'Pak Hari']);
DTI1.recentAdmin = 'Mas Dimas';
console.log(DTI1.recentAdmin);
