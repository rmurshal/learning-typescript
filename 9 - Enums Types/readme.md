# Enum

Enum adalah tipe data yang hanya ada di typescript yang berguna untuk menetapkan sebuah nilai yang tidak akan berubah pada sesuatu seperti object.

## Basic enum (start with value 0)

```ts
enum ROLES {
    ADMIN, // 0
    AUTHOR, // 1
    GUEST, // 2
}
```

## Custom enum

```ts
enum ROLES_2 {
    ADMIN = 10,
    AUTHOR, // 11
    GUEST, // 12
}
```

## Custom enum

```ts
enum ROLES_3 {
    ADMIN = 'admin',
    AUTHOR = 'author',
    GUEST = 'guest',
}
```

## Get enum value

```ts
const user = {
    name: 'Abdullah',
    role: ROLES.ADMIN,
}
```
