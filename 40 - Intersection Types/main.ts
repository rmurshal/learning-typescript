// ==== In Types

type TAdmin = {
    name: string
    roles: string[]
}

type TEmployee = {
    name: string
    startDate: Date
}

// Here TSuperEmployee will get all TAdmin & TEmployee property
type TSuperEmployee = TAdmin & TEmployee

// ==== In Interface

interface IAdmin {
    name: string
    roles: string[]
}

interface IEmployee {
    name: string
    startDate: Date
}

// Here ISuperEmployee will get all IAdmin & IEmployee property
interface ISuperEmployee extends IAdmin, IEmployee {}

// ==== Notes About "Type"

type numeric = number | string
type random = number | boolean

// Here universal only have the type that numeric and random has
// Or, a "number" type
type universal = numeric & random
