# Intersection

Adalah konsep dimana sebuah prototype digabungkan dengan prototype yang lain. Terdapat beberapa kondisi, antara lain:

### Pada Type

Intersection pada `type`, kita dapat menggunakan `&`

```ts
// ==== In Type

type TAdmin = {
    name: string
    roles: string[]
}

type TEmployee = {
    name: string
    startDate: Date
}

// Here TSuperEmployee will get all TAdmin & TEmployee property
type TSuperEmployee = TAdmin & TEmployee
```

### Pada Interface

Intersection pada `interface`, kita dapat menggunakan `extends`, sebagai berikut:

```ts
// ==== In Interface

interface IAdmin {
    name: string
    roles: string[]
}

interface IEmployee {
    name: string
    startDate: Date
}

// Here ISuperEmployee will get all IAdmin & IEmployee property
interface ISuperEmployee extends IAdmin, IEmployee {}
```

### Intersection in Basic Type

Perlu diketahui sebelumnya, bahwa perbedaan `interface` dengan `type` adalah type dapat digunakan pada variabel normal selain object, sedangkan interface umumnya digunakan pada object.

Berikut adalah contoh intersection pada type:

```ts
// ==== Notes About "Type"

type numeric = number | string
type random = number | boolean

// Here universal only have the type that numeric and random has
// Or, a "number" type
type universal = numeric & random
```
