# Creating Simple Angular Component

Decorator adalah konsep yang digunakan angular untuk membangun sebuah component. Berikut adalah contoh basicnya:

```ts
function WithTemplate(template: string, hookId: string) {
    // put "any" for constructor type so we can create an instance without error
    return function (constructor: any) {
        const hookEl = document.getElementById(hookId)
        const data = new constructor()
        if (hookEl) {
            hookEl.innerHTML = template
            setTimeout(() => {
                const h1 = hookEl.querySelector('h1')!
                h1.innerText = data.name
            }, 3000)
        }
    }
}

@WithTemplate('<h1>Hello, World!</h1>', 'app')
class Person {
    name = 'Blz'
    constructor() {
        console.log('Creating object...')
    }
}
```
