interface IPerson {
    name: string
    age: number
    hello(phrase: string): void
}

// implement interface for class with "implements"
class Person implements IPerson {
    constructor(public name: string, public age: number) {}
    hello(phrase: string): void {
        console.log(`${phrase} ${this.name}`)
    }
}

let leela: IPerson = new Person('leela', 30)
let ahmad: IPerson = new Person('john', 30)

// =====================================================

interface Personal {
    name: string
    age: number
}

interface Region {
    city: string
}

class People implements Personal, Region {
    constructor(public name: string, public age: number, public city: string) {}
}
