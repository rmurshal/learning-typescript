# Interface Pada Class

```ts
interface IPerson {
    name: string
    age: number
    hello(phrase: string): void
}

// implement interface for class with "implements"
class Person implements IPerson {
    constructor(public name: string, public age: number) {}
    hello(phrase: string): void {
        console.log(`${phrase} ${this.name}`)
    }
}
```

Btw kita juga bisa mengimplementasikan beberapa interface pada sebuah class dengan menambahkan `,`

```ts
interface Personal {
    name: string
    age: number
}

interface Region {
    city: string
}

// multiple interface has been implements
class People implements Personal, Region {
    constructor(public name: string, public age: number, public city: string) {}
}
```
