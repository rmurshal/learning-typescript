class Department {
    name: string

    constructor(name: string) {
        this.name = name
    }

    describe(this: Department) {
        console.log(`Department is ${this.name}`)
    }
}

let department = new Department('Information Technology')

department.describe() // Department is Information Technology

// must add name property here...
let departmentCopy = { describe: department.describe, name: 'Management' }

departmentCopy.describe() // Department is Management
