var Department = /** @class */ (function () {
    function Department(name) {
        this.name = name;
    }
    Department.prototype.describe = function () {
        console.log("Department is ".concat(this.name));
    };
    return Department;
}());
var department = new Department('Information Technology');
department.describe();
var departmentCopy = { describe: department.describe };
departmentCopy.describe();
