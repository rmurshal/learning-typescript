# This Keyword in Class

Kode berikut akan menghasilkan `undefined` pada fungsi `describe()` hasil copy object `department`:

```ts
class Department {
    name: string

    constructor(name: string) {
        this.name = name
    }

    describe() {
        console.log(`Department is ${this.name}`)
    }
}

let department = new Department('Information Technology')

department.describe() // Department is Information Technology

let departmentCopy = { describe: department.describe }

departmentCopy.describe() // Department is undefined
```

Untuk mengatasi hal ini, pada Javascript kita sebenarnya bisa menggunakan `bind()`. Tetapi pada Typescript kita dapat menggunakan `This: <type>` pada methods yang akan kita copy pada sebuah variabel, seperti berikut:

```ts
class Department {
    name: string

    constructor(name: string) {
        this.name = name
    }

    // add this here...
    describe(this: Department) {
        console.log(`Department is ${this.name}`)
    }
}

let department = new Department('Information Technology')

department.describe() // Department is Information Technology

// must add all property & methods that Department class has...
let departmentCopy = { describe: department.describe, name: 'Management' }

departmentCopy.describe() // Department is Management
```
