# Interface as Function Types

Interface juga dapat dijadikan sebagai prototype untuk membangun sebuah function, berikut contohnya:

```ts
interface addFunction {
    (a: number, b: number): number
}

let add: addFunction

// here the parameter must not be a and b
add = (x: number, y: number) => {
    return x + y
}
```

# Using Type

`Type` kurang lebih memiliki fungsi yang sama dengan `interface`, yaitu digunakan untuk membuat prototype sebuah variabel

### Type for Object

```ts
type Person = {
    name: string
    age: number
}

const john: Person = {
    name: 'john',
    age: 20,
}
```

### Type for Function

```ts
// here the return syntax is different!
type subsFunction = (a: number, b: number) => number
```
