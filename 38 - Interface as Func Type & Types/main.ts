interface addFunction {
    (a: number, b: number): number
}

let add: addFunction

add = (x: number, y: number) => {
    return x + y
}

// using types - for object

type Person = {
    name: string
    age: number
}

const john: Person = {
    name: 'john',
    age: 20,
}

// using types - for function

// here the return syntax is different!
type subsFunction = (a: number, b: number) => number
