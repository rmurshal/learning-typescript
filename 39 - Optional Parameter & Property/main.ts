interface IPerson {
    name: string
    age: number
    address?: string
}

const dehl = {
    name: 'dehl',
    age: 20,
    // ok without address
}

// ===== parameter

function addNumber(a: number, b: number, c?: number) {
    if (c) {
        return a + c
    } else {
        return a + b
    }
}
