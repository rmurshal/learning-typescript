# Optional Property

Untuk menggunakan optional, kita dapat menggunakan `?` setelah nama property pada prototypenya.

```ts
interface IPerson {
    name: string
    age: number
    address?: string
}

const dehl = {
    name: 'dehl',
    age: 20,
    // ok without address
}
```

# Optional Parameter

```ts
function addNumber(a: number, b: number, c?: number) {
    if (c) {
        return a + c
    } else {
        return a + b
    }
}
```
