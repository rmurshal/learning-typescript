class Department {
    private employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'Information Technology')
    }
}

// here, we only need 1 instance of AccountingDepartment class
class AccountingDepartment extends Department {
    // here the only 1 instance
    private static instance: AccountingDepartment

    // make it private, so we can control it only inside the class
    private constructor(id: string, public reports: string[]) {
        super(id, 'Accounting')
    }

    // the key here: create static function to get and create the instance
    // for static property, we can access here via this or AccountingDepartment
    static getInstance() {
        if (AccountingDepartment.instance) {
            return this.instance
        }
        this.instance = new AccountingDepartment('AC1', [
            'Report 1',
            'Report 2',
        ])
        return this.instance
    }

    addReports(report: string) {
        this.reports.push(report)
    }
}

// here are just the same thing
const accounting = AccountingDepartment.getInstance()
const accounting2 = AccountingDepartment.getInstance()

console.log(accounting, accounting2)
