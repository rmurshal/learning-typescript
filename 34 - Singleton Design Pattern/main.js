var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Department = /** @class */ (function () {
    function Department(id, name) {
        this.id = id;
        this.name = name;
        this.employees = [];
    }
    Department.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Department.prototype.printEmployees = function () {
        console.log(this.employees, this.employees.length);
    };
    Department.prototype.describe = function () {
        console.log("Department with id ".concat(this.id, " : ").concat(this.name));
    };
    return Department;
}());
var ITDepartment = /** @class */ (function (_super) {
    __extends(ITDepartment, _super);
    function ITDepartment(id, admins) {
        var _this = _super.call(this, id, 'Information Technology') || this;
        _this.admins = admins;
        return _this;
    }
    return ITDepartment;
}(Department));
// here, we only need 1 instance of AccountingDepartment class
var AccountingDepartment = /** @class */ (function (_super) {
    __extends(AccountingDepartment, _super);
    // make it private, so we can control it only inside the class
    function AccountingDepartment(id, reports) {
        var _this = _super.call(this, id, 'Accounting') || this;
        _this.reports = reports;
        return _this;
    }
    // the key here: create static function to get and create the instance
    // for static property, we can access here via this or AccountingDepartment
    AccountingDepartment.getInstance = function () {
        if (AccountingDepartment.instance) {
            return this.instance;
        }
        this.instance = new AccountingDepartment('AC1', [
            'Report 1',
            'Report 2',
        ]);
        return this.instance;
    };
    AccountingDepartment.prototype.addReports = function (report) {
        this.reports.push(report);
    };
    return AccountingDepartment;
}(Department));
// here are just the same thing
var accounting = AccountingDepartment.getInstance();
var accounting2 = AccountingDepartment.getInstance();
console.log(accounting, accounting2);
// here are different thing
var it1 = new ITDepartment('it1', ['a', 'b']);
var it2 = new ITDepartment('it2', ['c', 'd']);
console.log(it1, it2);
