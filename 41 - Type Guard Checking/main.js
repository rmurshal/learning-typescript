// ==== Checking for number
function getAdd(a, b) {
    if (typeof a === 'string' || typeof b === 'string') {
        // concat if a or b is a string
        return a.toString() + b.toString();
    }
    // here a or b exactly is a number
    return a + b;
}
function printEmployeeInfo(emp) {
    console.log("Name: ".concat(emp.name));
    // here the way we check if property contain in an object
    // using "in" property
    if ('roles' in emp) {
        console.log("Roles: ".concat(emp.roles));
    }
    if ('startDate' in emp) {
        console.log("startDate: ".concat(emp.startDate));
    }
}
var myEmployee = {
    name: 'Breeze',
    roles: ['Admin X'],
    startDate: new Date()
};
printEmployeeInfo(myEmployee);
// ==== Checking for class
var Car = /** @class */ (function () {
    function Car() {
    }
    Car.prototype.drive = function () {
        console.log('Driving car...');
    };
    return Car;
}());
var Truck = /** @class */ (function () {
    function Truck() {
    }
    Truck.prototype.drive = function () {
        console.log('Driving truck...');
    };
    Truck.prototype.loadingCargo = function (amount) {
        console.log("Loading cargo: ".concat(amount));
    };
    return Truck;
}());
function useVehicle(veh) {
    veh.drive();
    // here the another way to check if property or method inside of an object
    if (veh instanceof Truck) {
        veh.loadingCargo(20);
    }
}
var veh1 = new Car();
var veh2 = new Truck();
useVehicle(veh1);
useVehicle(veh2);
