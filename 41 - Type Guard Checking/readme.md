# Type Guard Checking

### For a number

```ts
// ==== Checking for number

type numeric = number | string

function getAdd(a: numeric, b: numeric) {
    if (typeof a === 'string' || typeof b === 'string') {
        // concat if a or b is a string
        return a.toString() + b.toString()
    }
}
```

### For a Object

```ts
// ==== Checking for object

interface IAdmin {
    name: string
    roles: string[]
}

interface IEmployee {
    name: string
    startDate: Date
}

type unknownEmployee = IAdmin | IEmployee

function printEmployeeInfo(emp: unknownEmployee) {
    console.log(`Name: ${emp.name}`)

    // here the way we check if property contain in an object
    // using "in" property

    if ('roles' in emp) {
        console.log(`Roles: ${emp.roles}`)
    }

    if ('startDate' in emp) {
        console.log(`startDate: ${emp.startDate}`)
    }
}

interface ISuperEmployee extends IAdmin, IEmployee {}

let myEmployee: ISuperEmployee = {
    name: 'Breeze',
    roles: ['Admin X'],
    startDate: new Date(),
}

printEmployeeInfo(myEmployee)
```

### For checking class

```ts
// ==== Checking for class

class Car {
    drive() {
        console.log('Driving car...')
    }
}

class Truck {
    drive() {
        console.log('Driving truck...')
    }

    loadingCargo(amount: number) {
        console.log(`Loading cargo: ${amount}`)
    }
}

type vehicle = Car | Truck

function useVehicle(veh: vehicle) {
    veh.drive()

    // here the another way to check if property or method inside of an object
    if (veh instanceof Truck) {
        veh.loadingCargo(20)
    }
}

const veh1 = new Car()
const veh2 = new Truck()

useVehicle(veh1)
useVehicle(veh2)
```
