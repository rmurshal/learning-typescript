var leelaPerson = {
    name: 'Leela',
    age: 30,
    hello: function (phrase) {
        console.log(phrase + this.name);
    }
};
var ahmadPerson = {
    name: 'Ahmad',
    age: 20,
    hello: function (phrase) {
        console.log("Welcome, ".concat(phrase, " ").concat(this.name));
    }
};
leelaPerson.hello('Halo ');
