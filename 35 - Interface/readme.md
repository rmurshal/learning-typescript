# Interface

Interface umumnya digunakan untuk menetapkan tipe data pada sebuah object.

```ts
interface Person {
    name: string
    age: number
    hello(phrase: string): void
}

// here leelaPerson and ahmadPerson must have all Person property and methods

let leelaPerson: Person = {
    name: 'Leela',
    age: 30,
    hello(phrase: string) {
        console.log(phrase + this.name)
    },
}

let ahmadPerson: Person = {
    name: 'Ahmad',
    age: 20,
    hello(phrase: string) {
        console.log(`Welcome, ${phrase} ${this.name}`)
    },
}

leelaPerson.hello('Halo ')
```
