/**
 * Index Type: Jika kita ingin menampung dynamic property
 */

interface IErrorContainer {
    // not must be "prop"
    [prop: string]: string

    // notes: if we want to add other property, the property value must be a string
    // becase it may raise a conflict with [prop: string]: string

    // example error:
    id: number
}

let errorBag: IErrorContainer = {
    email: 'Email is not valid',
    username: 'Username is required',
}
