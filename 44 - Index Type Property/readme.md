# Index Type Property

Digunakan jika kita ingin membuat prototype tipe data yang bersifat dinamis. Contoh untuk menampung list of error inside property

```ts
interface IErrorContainer {
    // not must be "prop"
    [prop: string]: string

    // notes: if we want to add other property, the property value must be a "string"
    // because it may raise a conflict with [prop: string]: string

    // example error:
    id: number
}

let errorBag: IErrorContainer = {
    email: 'Email is not valid',
    username: 'Username is required',
}
```
