interface course {
    title: string
    description: string
    addedDate: Date
}

type partialCourse = Partial<course>

let tsCourse: partialCourse = {
    title: 'Typescript Course',
    // "description" and "addedDate" is not required
}

function addCourse(
    title: string,
    description: string,
    addedDate: Date
): course {
    // Way 1 for insert data
    // let data: course = {
    //     title,
    //     description,
    //     addedDate
    // }

    // Way 2 for insert data, using partial, ie we need do some validation first to the property
    let data: Partial<course> = {}
    data.title = title
    data.description = description
    data.addedDate = addedDate

    // Way 1 for return data when partial is used
    // return data as course

    // Way 2 for return data when partial is used
    return <course>data
}

let city: Readonly<string[]> = ['Malang', 'Surabaya']
city.push('Jogjakarta')
