# Generics Utility Types

## Partial

Partial adalah utility types pada typescript yang digunakan untuk membuat sebuah tipe data dari tipe data lain, tetapi property-property yang ada di dalamnya bersifat optional.

Basic example:

```ts
interface course {
    title: string
    description: string
    addedDate: Date
}

type partialCourse = Partial<course>

let tsCourse: partialCourse = {
    title: 'Typescript Course',
    // "description" and "addedDate" is not required
}
```

Another example:

```ts
interface course {
    title: string
    description: string
    addedDate: Date
}

function addCourse(
    title: string,
    description: string,
    addedDate: Date
): course {
    // Way 1 for insert data, immediately pass all value to variable
    // let data: course = {
    //     title,
    //     description,
    //     addedDate
    // }

    // Way 2 for insert data, using partial, ie we need do some validation first to the property
    let data: Partial<course> = {}
    data.title = title
    data.description = description
    data.addedDate = addedDate

    // Way 1 for return data when partial is used
    // return data as course

    // Way 2 for return data when partial is used
    return <course>data
}
```

_Notes: Pada return value fungsi `addCourse`, kita tidak dapat langsung return variabel `data`, karena tipe datanya adalah `Partial<course>`, sehingga perlu diubah terlebih dahulu. Untuk mengubahnya tersebut terdapat 2 cara. Cara 1 sepertinya terkait dengan konsep type casting, yaitu mengubah tipe data dari sebuah variabel menjadi yang kita inginkan. Sedangkan cara 2 terkait dengan konsep type assertion, yaitu memberitahu typescript mengenai tipe data pada variabel tersebut._

## Readonly

Yaitu utility type yang digunakan untuk menjadikan variabel hanya dapat diread saat sudah diinisiasi pertama kali.

```ts
let city: Readonly<string[]> = ['Malang', 'Surabaya']
// Error
city.push('Jogjakarta')
```
