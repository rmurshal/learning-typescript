# Intro to TypeScript

- Typescript adalah pengembangan dari Javascript.
- Di develop oleh Microsoft pada 2012.
- Typescript bukan bahasa pemrograman, karena pada akhirnya akan dicompile kembali ke Javascript.
- Typescript: Strongly typing language, mencegah runtime error.
- Contoh penggunaan Typescript:
  - Angular (base code angular dibangun menggunakan Typescript)
  - React
  - Vue.js
- Pada Typescript kita juga bisa tulis kode Javascript.
- Dokumentasi: https://www.typescriptlang.org/docs/
- Contoh masalah pada Javascript:

```js
function add(a, b) {
  return a + b;
}
console.log(add("1" + "2")); // 12
```
