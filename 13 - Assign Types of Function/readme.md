# Assign Types of Function

```ts
function add(num1: number, num2: number): number {
    return num1 + num2
}

function greetings(name: string): void {
    console.log(`Hello ${name}`)
}

let operations: (num1: number, num2: number): number

// error
operations = greetings

// ok
operations = add
```
