# Lib Property

Properti `lib` yang ada pada `compilerOptions` berguna untuk menspesifikkan library javascript yang akan kita gunakan dalam project. Saat kita uncomment, kita harus spesifikkan apa yang akan digunakan karena `[]` berarti tidak ada library.

## Error - lib null

```json
"target": "ES5",
"lib": []
```

```ts
const button = document.querySelector('button')! // can't find 'document' (DOM lib)

// can't find 'addEventListener' (DOM lib)
button.addEventListener('click', () => {
    console.log('button clicked')
})

const myMap = new Map() // can't find Map() (ES6 lib)
```

## Success - library included

```json
"target": "ES5",
"lib": ["DOM", "ES6"]
```

```ts
const button = document.querySelector('button')! // ok

// ok
button.addEventListener('click', () => {
    console.log('button clicked')
})

const myMap = new Map() // ok
```
