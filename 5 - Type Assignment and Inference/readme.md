# Type Assignment
Menetapkan tipe data dari sebuah variabel **secara spesifik**.

```ts
let myStr: string = 'Hello'
let myNum: number = 123
let myBool: boolean = true
```

# Type Inference
Menetapkan tipe data dari sebuah variabel **oleh Typescript**.

```ts
let myStr = 'Hello' // string
let myNum = 123 // number
let myBool = true // boolean
```

***Notes**: Variabel yang tipe datanya sudah ditetapkan, hanya bisa diberi nilai baru sesuai tipe datanya.*
