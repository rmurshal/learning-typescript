# Returning Constructor from Class Decorator

Sebagai review, decorator digunakan untuk memodifikasi perilaku dari sebuah fungsi atau class (dengan mereturn targetnya tersebut).

```ts
function Logger(logString: string) {
    console.log('Logger factory')
    return function (constructor: Function) {
        console.log(logString)
        console.log(constructor)
    }
}

function WithTemplate(template: string, hookId: string) {
    console.log('WithTemplate factory')
    return function (constructor: any) {
        console.log('Hello from WithTemplate decorator')
        const hookEl = document.getElementById(hookId)
        const data = new constructor()
        if (hookEl) {
            hookEl.innerHTML = template
            setTimeout(() => {
                const h1 = hookEl.querySelector('h1')!
                h1.innerText = data.name
            }, 3000)
        }
        // RETURN OPTIONS
    }
}

@Logger('Hello from Logger decorator')
@WithTemplate('<h1>Hello, World!</h1>', 'app')
class Person {
    name = 'Blz'
    constructor() {
        console.log('Creating object...')
    }
}
```

Berikut adalah kombinasi opsi return pada komentar `RETURN OPTIONS` yang dapat dilakukan:

_Notes: Pada dasarnya kita harus mereturn anonymous class sebagai wujud dari class Person, dimana seluruh property atau method yang ada di dalamnya harus kita cantumkan._

-   Return dengan hanya mengubah property / method pada class

```ts
return class {
    name = 'John'
}
```

Dengan menggunakan kombinasi ini, property name yang awalnya `Blz` berubah menjadi `John`. Tetapi innerText dari element `h1` yang ada di dalamnya tetap menggunakan `Blz` karena proses perubahan elemen terdapat di bagian bawah decorator.

-   Return dengan menambahkan property / method pada class

```ts
return class extends constructor {
    name = 'John'
    age = 29
}
```

Dengan menggunakan kombinasi ini, kita tidak perlu menuliskan seluruh properti atau method yang ada di dalam class Person karena sudah melakukan `extends`. Kita juga dapat memodifikasi property / method yang sudah ada sebelumnya, serta dapat menambahkan property / method baru pada block return tersebut.
