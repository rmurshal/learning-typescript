# Generics Functions

## Contoh 1

Berikut adalah contoh membangun sebuah function dengan bantuan generics:

```ts
function mergeObject<T extends object, U>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

const data = mergeObject({ name: 'Bilz' }, { age: 20 })

console.log(data)
```

Penjelasan:

-   Fungsi `mergeObject` menerima dua parameter `objA` dan `objB` yang memiliki tipe `T` dan `U`.
-   Tipe `T` dan `U` ini adalah sebuah `generics` (yang memungkinkan typescript mendeteksi tipe data variabel tersebut) pada bagian `<>`, dimana `T` melakukan extends pada tipe `object` agar typescript mengetahui return type dari fungsi `mergeObject` ini sebagai object (karena fungsi `Object.assign()`).
-   Tanpa menggunakan generics ini, variabel `data` tidak akan mendapatkan referensi property apa saja yang ada di dalamnya.

## Contoh 2

Contoh menspesifikkan argumen generics:

```ts
function mergeObject<T extends object, U>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

// Specify the type of arguments here...
const data = mergeObject<{ name: string; hobbies: string[] }, { age: number }>(
    // First args need hobbies now
    { name: 'Bilz', hobbies: ['cooking'] },
    { age: 20 }
)

console.log(data)
```

Penjelasan:

-   Saat pemanggilan fungsi `mergeObject`, kita kembali menggunakan generics untuk menspesifikkan property apa saja yang akan ada pada argumen yang akan di passing.
