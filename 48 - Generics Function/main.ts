function mergeObject<T extends object, U>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

const data = mergeObject<{ name: string; hobbies: string[] }, { age: number }>(
    // First args need hobbies now
    { name: 'Bilz', hobbies: ['cooking'] },
    { age: 20 }
)

console.log(data)
