# Unknown types

Tipe `unknown` digunakan saat kita `tidak tahu` tipe data apa yang akan ditetapkan pada sebuah variabel, tetapi typescript tidak melepas kendali dari variabel tersebut. Berbeda dengan `any`, dimana typescript sudah give up untuk mengontrol variabel tersebut.

```ts
// example unknown
let userInput: unknown
let userName: string
userInput = 12
userInput = 'name'
userInput2 = 12
userInput2 = 'name'
// error
userName = userInput
// ok
if (typeof userInput === 'string') {
    userName = userInput
}
```

```ts
// example any
let userInput: any
let userName: string
userInput = 12
userInput = 'name'
// ok
userName = userInput
```

# Never types

Ada saat dimana function benar-benar tidak akan me-return apapun bahkan `undefined` (contoh: throw sebuah error). Dalam kasus ini, kita dapat menggunakan tipe `never`.

```ts
// contoh never
function generateError(message: string, code: number): never {
    throw { message, code }
}
generateError('Invalid page', 500) // only uncaught in console, no undefined

// contoh any
function generateGreet(people: string): void {
    console.log(`hello good morning ${people}`)
}
console.log(generateGreet('ahmad')) // there is undefined in console
```
