# Decorator for Accessor, Method and Parameter

Sama seperti pada class dan property, kita juga dapat menggunakan decorator pada accessor (Set), method, dan parameter yang ada di dalam sebuah class. Berikut adalah contoh serta parameter apa saja yang di dapatkan:

```ts
/**
 * For Property
 *
 * Target: prototype instance (method)
 * Property name: key dari property yang akan di decorate
 *
 * Sebenarnya kita bisa running setter disini untuk mengubah nilai dari property yang di decorate
 */
function Log(target: any, propertyName: string) {
    console.log('Log - For Property')
    console.log(target)
    console.log(propertyName)
    console.log('===============')
}

/**
 * For Accessor
 *
 * Target: prototype instance (method)
 * Property name: key dari property yang akan di decorate
 * Descriptor: configuration of accessor (configurable, enumerable, get, set)
 */
function Log2(target: any, name: string, descriptor: PropertyDescriptor) {
    console.log('Log2 - For Accessor')
    console.log(target)
    console.log(name)
    console.log(descriptor)
    console.log('===============')
}

/**
 * For Method
 *
 * Target: prototype instance (method)
 * Property name: key dari property yang akan di decorate
 * Descriptor: configuration of accessor (configurable, enumerable, value, writable)
 */
function Log3(target: any, name: string, descriptor: PropertyDescriptor) {
    console.log('Log3 - For Method')
    console.log(target)
    console.log(name)
    console.log(descriptor)
    console.log('===============')
}

/**
 * For Parameter
 *
 * Target: prototype instance (method)
 * Property name: key dari property yang akan di decorate
 * Position: position of the parameter
 */
function Log4(target: any, name: string, position: number) {
    console.log('Log4 - For Parameter')
    console.log(target)
    console.log(name)
    console.log(position)
    console.log('===============')
}

class Product {
    @Log
    title: string
    private _price: number

    @Log2
    set price(value: number) {
        if (value > 0) {
            this._price = value
        } else {
            throw new Error('Price should be a positive number')
        }
    }

    constructor(title: string, price: number) {
        this.title = title
        this._price = price
    }

    @Log3
    getPriceWithTax(@Log4 tax: number) {
        return this._price * tax
    }
}
```
