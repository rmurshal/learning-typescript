class Department {
    private employees: string[] = []

    // `id` and `name` will be available as property inside this class
    constructor(private id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

let department = new Department('DTI', 'Information Technology')
department.describe() // Department with id DTI : Information Technology
