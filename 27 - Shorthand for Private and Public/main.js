var Department = /** @class */ (function () {
    // `id` and `name` will be available as property inside this class
    function Department(id, name) {
        this.id = id;
        this.name = name;
        this.employees = [];
    }
    Department.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Department.prototype.printEmployees = function () {
        console.log(this.employees, this.employees.length);
    };
    Department.prototype.describe = function () {
        console.log("Department with id ".concat(this.id, " : ").concat(this.name));
    };
    return Department;
}());
var department = new Department('DTI', 'Information Technology');
department.addEmployee('John');
department.addEmployee('Chris');
department.printEmployees();
department.describe();
