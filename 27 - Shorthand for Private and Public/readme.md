# Shorthand for Private and Public Access Modifier

Dibanding menginisiasi satu-satu setiap property yang ada pada sebuah class beserta access modifiernya dan meng-assign nilai pada constructor seperti ini:

```ts
class Department {
    private id: string
    public name: string

    constuctor(id: string, name: string) {
        this.id = id
        this.name = name
    }
}
```

Pada typescript kita dapat menyingkatnya dengan cara seperti ini:

```ts
class Department {
    constructor(private id: string, public name: string) {}
}
```

Dengan menggunakan shorthand di atas, variabel `id` dan `name` selanjutnya akan availabel sebagai property yang ada pada class.

_Notes: Kita **WAJIB** untuk memberi access modifier baik `public` atau `private` pada parameter constructor agar variabel menjadi sebuah property pada class. Apabila tidak, variabel tersebut tidak akan menjadi property pada class._

## Example basic

```ts
class Department {
    private employees: string[] = []

    // `id` and `name` will be available as property inside this class
    constructor(private id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

let department = new Department('DTI', 'Information Technology')
department.describe() // Department with id DTI : Information Technology
```
