# Return type of Method Decorator

Berikut adalah contoh melakukan return pada sebuah method decorator. Method decorator dibawah berguna untuk melakukan binding terhadap class, sehingga ketika sebuah method dipanggil pada variabel lain atau pada sebuah timer / event javascript, dia masih memiliki referensi dengan kelas asalnya:

```ts
function AutoBind(target: any, name: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value

    const descr: PropertyDescriptor = {
        configurable: true,
        enumerable: false,
        get() {
            // we can use this here because AutoBind is called inside Person class
            return originalMethod.bind(this)
        },
    }

    return descr
}

class Person {
    message = 'Hi, Ramz!'

    @AutoBind
    getMessage() {
        console.log(this.message)
    }
}

const button = document.querySelector('button')!
const p = new Person()

// so we don't need bind here to not get undefined
button.addEventListener('click', p.getMessage)
```
