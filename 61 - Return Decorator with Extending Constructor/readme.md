# Return Decorator with Extending Constructor

Pada contoh kali ini, kita akan melakukan hook element `h1` pada return sebuah decorator serta mendapatkan data dari property name melalui this (bukan menginisiasi melalui new constructor).

Berbeda dengan pada materi nomor 60, yang mana proses hook dilakukan sebelum return decorator function serta proses returnnya tidak begitu kompleks. Hal ini membuat proses hook hanya dapat terjadi ketika kita telah membuat sebuah instance pada class.

```ts
function Logger(logString: string) {
    console.log('Logger factory')
    return function (constructor: Function) {
        console.log(logString)
        console.log(constructor)
    }
}

function WithTemplate(template: string, hookId: string) {
    console.log('WithTemplate factory')
    // Kita harus mengubah tipe data dari constructor menjadi sebuah function, yang mana function tersebut harus me-return property name (karena di definisikan pada class)
    return function <T extends { new (...args: any[]): { name: string } }>(
        constructor: T
    ) {
        console.log('Hello from WithTemplate decorator')
        return class extends constructor {
            // Dapatkan semua parameter constructor
            constructor(...args: any[]) {
                // Tidak perlu pass karena dalam hal ini tidak terdapat parameter yang akan digunakan
                super()
                const hookEl = document.getElementById(hookId)
                if (hookEl) {
                    hookEl.innerHTML = template
                    setTimeout(() => {
                        const h1 = hookEl.querySelector('h1')!
                        // Sehingga disini kita dapat menggunakan this
                        h1.innerText = this.name
                    }, 3000)
                }
            }
        }
    }
}

@Logger('Hello from Logger decorator')
@WithTemplate('<h1>Hello, World!</h1>', 'app')
class Person {
    name = 'Blz'
    constructor() {
        console.log('Creating object...')
    }
}

// Optional
const person = new Person()
console.log(person)
```

Notes: Fungsi decorator WithTemplate diatas akan me return sebuah class baru yang sangat dimodifikasi, dimana di dalamnya terdapat pemanggilan constructor yang digunakan untuk melakukan mounting element `h1`.

Optional:

-   Jika kita tidak membuat instance kelas `Person`, maka proses mounting tidak akan berjalan
-   Jika kita membuat instance kelas `Person`, maka proses mounting akan berjalan
