var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var DataStorage = /** @class */ (function () {
    function DataStorage() {
        this.data = [];
    }
    DataStorage.prototype.addItem = function (item) {
        this.data.push(item);
    };
    DataStorage.prototype.removeItem = function (item) {
        this.data.splice(this.data.indexOf(item), 1);
    };
    DataStorage.prototype.getItems = function () {
        return __spreadArray([], this.data, true);
    };
    return DataStorage;
}());
var stringStorage = new DataStorage();
stringStorage.addItem('Satu');
stringStorage.addItem('Dua');
stringStorage.removeItem('Satu');
console.log(stringStorage.getItems());
var numberStorage = new DataStorage();
numberStorage.addItem(1);
numberStorage.addItem(2);
numberStorage.removeItem(1);
console.log(numberStorage.getItems());
// Notes yang ke delete adalah saber
var objectStorage = new DataStorage();
objectStorage.addItem({ name: 'Miya' });
objectStorage.addItem({ name: 'Saber' });
objectStorage.removeItem({ name: 'Miya' });
console.log(objectStorage.getItems());
// Fix yang ke delete sudah Miya
var fixObjectStorage = new DataStorage();
var obj = { name: 'Miya' };
fixObjectStorage.addItem(obj);
fixObjectStorage.addItem({ name: 'Saber' });
fixObjectStorage.removeItem(obj);
console.log(fixObjectStorage.getItems());
