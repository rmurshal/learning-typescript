// T --> Tell the typescript what of data we want to store when init the object
class DataStorage<T> {
    private data: Array<T> = []

    addItem(item: T) {
        this.data.push(item)
    }

    removeItem(item: T) {
        this.data.splice(this.data.indexOf(item), 1)
    }

    getItems() {
        return [...this.data]
    }
}

// Example 1
let stringStorage = new DataStorage<string>()
stringStorage.addItem('Satu')
stringStorage.addItem('Dua')
stringStorage.removeItem('Satu')
console.log(stringStorage.getItems())

// Example 2
let numberStorage = new DataStorage<number>()
numberStorage.addItem(1)
numberStorage.addItem(2)
numberStorage.removeItem(1)
console.log(numberStorage.getItems())

// Example 3
let objectStorage = new DataStorage<object>()
objectStorage.addItem({ name: 'Miya' })
objectStorage.addItem({ name: 'Saber' })
objectStorage.removeItem({ name: 'Miya' })
console.log(objectStorage.getItems())

// Example 4
let fixObjectStorage = new DataStorage<object>()
const obj = { name: 'Miya' }
fixObjectStorage.addItem(obj)
fixObjectStorage.addItem({ name: 'Saber' })
fixObjectStorage.removeItem(obj)
console.log(fixObjectStorage.getItems())
