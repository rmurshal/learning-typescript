# Generics with Classes

Berikut adalah contoh menggunakan generics dengan class untuk membangun sebuah data storage yang tipe datanya harus dispesifikkan saat ingin digunakan

```ts
// T --> Tell the typescript what of data we want to store when init the object
class DataStorage<T> {
    private data: Array<T> = []

    addItem(item: T) {
        this.data.push(item)
    }

    removeItem(item: T) {
        this.data.splice(this.data.indexOf(item), 1)
    }

    getItems() {
        return [...this.data]
    }
}

// Example 1
let stringStorage = new DataStorage<string>()
stringStorage.addItem('Satu')
stringStorage.addItem('Dua')
stringStorage.removeItem('Satu')
console.log(stringStorage.getItems())

// Example 2
let numberStorage = new DataStorage<number>()
numberStorage.addItem(1)
numberStorage.addItem(2)
numberStorage.removeItem(1)
console.log(numberStorage.getItems())

// Example 3
let objectStorage = new DataStorage<object>()
objectStorage.addItem({ name: 'Miya' })
objectStorage.addItem({ name: 'Saber' })
objectStorage.removeItem({ name: 'Miya' })
console.log(objectStorage.getItems())

// Example 4
let fixObjectStorage = new DataStorage<object>()
const obj = { name: 'Miya' }
fixObjectStorage.addItem(obj)
fixObjectStorage.addItem({ name: 'Saber' })
fixObjectStorage.removeItem(obj)
console.log(fixObjectStorage.getItems())
```

Tanpa menggunakan generics, property `data` yang ada pada class `DataStorage` mungkin hanya dapat menerima spesifik data seperti `Array<string>`, atau dapat juga menggunakan union type seperti `Array<string | number>`. Hal tersebut tentulah tidak cukup efisien dalam mengembangkan sebuah class. Sehingga, kita dapat menggunakan generics untuk mengatasi permasalahan ini.

_Notes: Pada Example 3, data yang terhapus adalah `{ name: 'Saber' }` meskipun kita mencoba untuk menghapus `{ name: 'Miya' }`. Hal ini terjadi karena method `indexOf` akan mengembalikan nilai `-1` saat data yang ingin dicari tidak ketemu. Dengan menggunakan nilai `-1` pada method `splice()` berarti akan menghapus elemen terakhir pada array tersebut. Hal ini terkait dengan konsep reference type dan value type pada javascript. Untuk mencegah error tersebut, kita dapat menggunakan `Example 4`_

Kita juga dapat membatasi tipe data apa saja yang dapat disimpan pada class `DataStorage` dengan menggunakan:

```ts
class DataStorage<T extends string | number | boolean> {}
```

Dengan begini, class `DataStorage` kali ini hanya akan menyimpan type value saja pada property `data` yang ada di dalamnya.
