function mergeObject<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

const data = mergeObject({ name: 'Bilz', hobbies: ['cooking'] }, { age: 20 })

console.log(data)
