# Generics Function with Constraint

Berdasarkan contoh sebelumnya:

```ts
function mergeObject<T extends object, U>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

const data = mergeObject({ name: 'Bilz', hobbies: ['cooking'] }, { age: 20 })

console.log(data)
```

Kita dapat mengirimkan argumen kedua sebagai hanya sebuah number, contoh: `mergeObject({ name: 'Bilz', hobbies: ['cooking'] }, 20)` dan typescript tidak akan memberikan error. Hal ini terjadi karena `U` bersifat general dan tidak membatasi tipe data apa yang akan diterimanya. Hasil log variabel `data` juga hanya terdapat property `name` dan `hobbies`.

Untuk membatasi hal tersebut, kita bisa menspesifikkan bahwa `U` hanya akan menerima sebuah object dengan menggunakan keyword `extends` seperti berikut:

```ts
function mergeObject<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

const data = mergeObject({ name: 'Bilz', hobbies: ['cooking'] }, { age: 20 })

console.log(data)
```

Dengan demikian, argumen kedua dari fungsi `mergeObject()` tersebut harus berupa object, apapun property yang ada di dalamnya.
