# DOM dan Typecasting pada Typescript

Beberapa hasil dari catch element HTML:

```ts
const paragraph = document.querySelector('p') // HTMLParagraphElement | null
const paragraph2 = document.querySelector('#paragraph2') // Element | null
const paragraph3 = document.getElementById('paragraph3') // HTMLElement | null
```

Mendapatkan element HTML lalu menjalankan method yang ada di dalamnya:

### Error - No "value" property inside "errorInput"

```ts
const errorInput = document.getElementById('errorInput')
errorInput.value = 'Error Input'
```

### Cara 1

```ts
const myInput = <HTMLInputElement>document.getElementById('input')
myInput.value = 'Some Input'
```

### Cara 2

```ts
const myInput2 = document.getElementById('input2') as HTMLInputElement
myInput2.value = 'Some Input 2'
```

### Cara 3

```ts
const myInput3 = document.getElementById('input3')
if (myInput3) {
    ;(myInput3 as HTMLInputElement).value = 'Some Input 3'
}
```
