# Example Error

```ts
let person: object = {
  name: 'Ahmad',
  age: 20
}
console.log(person.name) // Error, Typescript doesn't know name property inside person object.
```

# Best Practice

```ts
let person2: {
  name: string
  age: number
  address: 'Surabaya' // Tips: Must have address: 'Surabaya'
} = {
  name: 'Ahmad',
  age: 20,
  address: 'Surabaya'
}
console.log(person2.name) // No Error
```
