# Return Types for other Decorators

Berikut adalah contoh untuk property decorator:

```ts
function positiveNumber(target: Object, propertyKey: string) {
    let value = target[propertyKey]

    const getter = function () {
        return value
    }

    const setter = function (newValue: number) {
        if (newValue <= 0) {
            throw new Error('Value must be positive.')
        }

        value = newValue
    }

    Object.defineProperty(target, propertyKey, {
        get: getter,
        set: setter,
    })
}

class Example {
    @positiveNumber
    public num: number

    constructor(num: number) {
        this.num = num
    }
}

const ex = new Example(5)
console.log(ex.num) // Output: 5

ex.num = -1 // Throws an error
```

Sebenarnya pada contoh di atas, tidak terdapat return statement untuk property decorator. Kita hanya menambahkan validasi untuk property `num` yang ada agar dia hanya dapat diubah dengan nilai positif.
