# Funtion return

## Example return number

```ts
function add(num1: number, num2: number): number {
    return num1 + num2
}
```

## Example return nothing

```ts
function printName(name: string): void {
    console.log(`Your name is ${name}`)
}
```

## Example return undefined

```ts
function printAge(age: number): undefined {
    console.log(`Your age is ${age}`)
    return // must put return statemnet here, if not ts will throw an error
}
```

## Notes

The reason why console will throw `undefined` when call function inside `console.log()` is because it return nothing / `undefined`.
