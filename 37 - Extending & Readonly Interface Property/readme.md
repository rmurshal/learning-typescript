# Readonly Property

```ts
interface IPerson {
    readonly name: string
    age: number
    hello(phrase: string): void
}

class Person implements IPerson {
    // here name is normal here
    constructor(public name: string, public age: number) {}
    hello(phrase: string): void {
        console.log(`${phrase} ${this.name}`)
    }
}

// but name in "leela" here is readonly!
let leela: IPerson = new Person('leela', 30)
```

# Extending Interface

```ts
interface IName {
    name: string
}

// here IPerson extends IName
interface IPerson extends IName {
    age: number
    city: string
}

class People implements IPerson {
    constructor(public name: string, public age: number, public city: string) {}
}
```

Kita juga dapat menambahkan interface lain yang akan di extends pada sebuah interface dengan menambahkan `,`, contoh:

```ts
interface IName {
    name: string
}

interface IAge {
    age: number
}

interface IPerson extends IName, IAge {
    city: string
}
```

_Notes: Konsep multiple extends ini hanya berlaku pada extends interface dan tidak berlaku pada extends class_
