interface IPerson {
    // here
    readonly name: string
    age: number
    hello(phrase: string): void
}

class Person implements IPerson {
    constructor(public name: string, public age: number) {}
    hello(phrase: string): void {
        console.log(`${phrase} ${this.name}`)
    }
}

let leela: IPerson = new Person('leela', 30)

// =====================================================

interface IName {
    name: string
}

// here IPerson2 extends IName
interface IPerson2 extends IName {
    age: number
    city: string
}

class People implements IPerson2 {
    constructor(public name: string, public age: number, public city: string) {}
}
