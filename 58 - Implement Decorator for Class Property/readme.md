# Decorator for Class Property

```ts
/**
 * Target: prototype instance (method)
 * Property name: key dari property yang akan di decorate
 *
 * Sebenarnya kita bisa running setter disini untuk mengubah nilai dari property yang di decorate
 */
function Log(target: any, propertyName: string) {
    console.log(target)
    console.log(propertyName)
}

class Product {
    @Log
    title: string
    private _price: number

    set price(value: number) {
        if (value > 0) {
            this._price = value
        } else {
            throw new Error('Price should be a positive number')
        }
    }

    // Basically we can use the shorthand notations too
    constructor(title: string, price: number) {
        this.title = title
        this._price = price
    }

    getPriceWithTax(tax: number) {
        return this._price * tax
    }
}

const newProduct = new Product('New Product', 12)
console.log(newProduct)
```

Sehingga, bisa dilihat perbedaan saat menggunakan decorator pada class dan property:

-   Pada class akan mendapatkan constructor
-   Pada property akan mendapatkan target dan property name nya
