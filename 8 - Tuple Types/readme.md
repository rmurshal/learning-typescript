# Pengertian

Tuple adalah array yang hanya bisa berisi n value yang tipe datanya sudah ditetapkan.

## Contoh membuat tuple yang salah

```ts
// Typescript otomatis menyimpulkan tipe data role sebagai (number | string)[]

let role = [1, 'Admin']
```

## Contoh membuat tuple yang benar

```ts
let role2: [number, string] = [1, 'Admin']
```

## Contoh menggunakan tuple yang salah

```ts
role2[0] = 'Admin' // Harusnya number
role2[1] = 1 // Harusnya string
```

## Exception pada tuple

```ts
role2.push(2)

// Compiler typescript tidak mengenali method .push() sehingga tidak memberikan runtime error
// Nilai 2 juga akan dipush ke dalam variabel role2
```
