# Array Types

```ts
// No Error
let myArr: string[] = ['Satu', 'Dua']

// Error
let myArr2: string[] = ['Satu', 2]
```
