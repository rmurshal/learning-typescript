// Variables
let myNumber: number = 123
let myString: string = 'Hello, World!'
let myBoolean: boolean = true

// Variables
let myObject: { name: string; age: number } = { name: 'John, Doe', age: 30 }
let myArray: number[] = [1, 2, 3, 4]
let myTuple: [string, number] = ['Rama', 123]
let myAny: any = 'Hello, World!'
let myUnion: string | number = 'Hello, World!'
let myLiteral: 'admin' | 'user' = 'admin'
let myUnknown: unknown = 'Hello, World!'

// Enums
enum myEnums {
    Admin, // 0
    Author, // 1
    Guest, // 2
}

// Function
function add(num1: number, num2: number): number {
    return num1 + num2
}
function generateError(message: string, code: number): never {
    throw { message, code }
}
