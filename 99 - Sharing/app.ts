let myName: string = 'John Doe'
console.log(myName)

class Person {
    // name and phoneNumber will become property inside Person Class
    constructor(public name: string, private phoneNumber: number) {}
}

// we can't create instance of Something class
// we can only extend it
abstract class Something {}

interface Person {
    name: string
    age: number
    city: string
    isMarry: boolean
    sayHello: () => string
}

let cities: Array<string> = []

function getArrayLength<T>(arr: T[]): number {
    return arr.length
}
