class Department {
    name: string
    private employees: string[] = []

    constructor(name: string) {
        this.name = name
    }

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department is ${this.name}`)
    }
}

let department = new Department('Information Technology')

department.addEmployee('John')
department.addEmployee('Chris')

// Error
department.employees[2] = 'Michele'

department.printEmployees()
