var Department = /** @class */ (function () {
    function Department(name) {
        this.employees = [];
        this.name = name;
    }
    Department.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Department.prototype.printEmployees = function () {
        console.log(this.employees, this.employees.length);
    };
    Department.prototype.describe = function () {
        console.log("Department is ".concat(this.name));
    };
    return Department;
}());
var department = new Department('Information Technology');
department.addEmployee('Rama');
department.addEmployee('Azkun');
department.printEmployees();
