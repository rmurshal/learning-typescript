# Private and Public Access Modifier

Secara default, seluruh property dan methods yang ada pada sebuah class akan memiliki access modifier sebagai `public`. Access modifier sebagai `public` berarti property atau method tersebut dapat diakses di dalam maupun di luar class. Sedangkan apabila property atau method memiliki access modifier sebagai `private`, dia hanya bisa diakses di dalam class nya itu saja.

```ts
class Department {
    name: string
    private employees: string[] = []

    constructor(name: string) {
        this.name = name
    }

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department is ${this.name}`)
    }
}

let department = new Department('Information Technology')

department.addEmployee('John')
department.addEmployee('Chris')

// Error
department.employees[2] = 'Michele'

department.printEmployees()
```

_Notes: Pada normal javascript, **tidak** terdapat fitur access modifier ini._
