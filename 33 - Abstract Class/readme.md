# Abstract Class dan Abstract Method

-   Abstract Class: Memungkinkan kita untuk menjadi sebuah class tidak dapat dibuat instancenya (hanya dapat dijadikan sebagai parent class).
-   Abstract Method: Memungkinkan kita untuk menset sebuah method pada parent class yang harus diimplementasikan pada child classnya.

```ts
// Department class can't create an instance
abstract class Department {
    protected employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }

    // don't need {} here...
    abstract showPrestation(): string
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'Information Technology')
    }

    addEmployee(employee: string): void {
        if (employee === 'Pak Hari') {
            return
        }
        this.employees.push(employee)
    }

    // must add showPrestation here...
    showPrestation() {
        return 'Prestasi: Juara Gemastik'
    }
}

const DTI1 = new ITDepartment('DTI1', ['Pak Agus', 'Pak Hari'])
```
