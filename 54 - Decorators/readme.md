# Decorator

Decorator adalah sebuah fungsi yang menerima parameter dan dapat digunakan untuk menambah fungsionalitas sebuah class, property, ataupun method. Fitur decorator ini hanya dapat digunakan (dengan memberi nilai `true` pada property `experimentalDecorators` yang ada pada `tsconfig.json`) pada project based typescript dan tidak bisa dikompilasi secara manual.

Berikut adalah contoh decorator yang melakukan extends terhadap sebuah class:

```ts
function Logger(constructor: Function) {
    console.log('Logging the data')
    console.log(constructor)
}

// Here we extends the class with decorator
@Logger
class Person {
    name = 'Blz'

    constructor() {
        console.log('Creating object...')
    }
}

const person = new Person()
console.log(person)

/**
 * Hasil log 1 - Dengan membuat instance:
 * Logging the data
 * class Person { with the all content }
 * Creating object...
 * person object
 *
 * Hasil log 2 - Tanpa membuat instance:
 * Logging the data
 * class Person { with the all content }
 */
```
