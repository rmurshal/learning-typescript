function extractFromObject<T extends object, U extends keyof T>(
    obj: T,
    key: U
) {
    return obj[key]
}

extractFromObject({ name: 'Bono' }, 'name')
