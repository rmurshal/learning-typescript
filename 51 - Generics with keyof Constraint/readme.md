# Generics with keyof Constraint

Berikut adalah contoh untuk memastikan apakah sebuah key merupakan member dari sebuah object menggunakan `keyof` constraint pada `generics`:

```ts
function extractFromObject<T extends object, U extends keyof T>(
    obj: T,
    key: U
) {
    return obj[key]
}

extractFromObject({ name: 'Bono' }, 'name')
```

Tanpa menggunakan `generics` tersebut, typescript tidak akan memberi peringatan error saat kita melakukan:

```ts
extractFromObject({}, 'name')
```

Yang mana property `name` belum pasti ada pada object `{}`
