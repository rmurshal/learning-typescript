# Readonly property

Adalah property yang hanya bisa di read baik di dalam maupun di luar sebuah class. Property ini hanya dapat diberikan sebuah nilai saat menginisiasi sebuah object.

_Notes: Property `readonly` ini hanya support pada typescript dan tidak pada javascript._

```ts
class Department {
    private employees: string[] = []

    // add "readonly" keyword here...
    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        // error
        this.id = 'DTC'
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}
```
