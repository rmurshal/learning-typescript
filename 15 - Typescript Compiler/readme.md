# Kompilasi satu file

Untuk melakukan kompilasi satu file secara `otomatis`, dapat menggunakan perintah:

```
tsc <path to file> --watch
```

Atau:

```
tsc <path to file> -w
```

Untuk menargetkan hasil kompilasi javascript environtmentnya, dapat menggunakan perintah:

```
tsc -t es5 <path to file>
```

# Kompilasi project

Inisiasi project dengan menggunakan perintah:

```
tsc --init
```

Kemudian akan terbentuk sebuah `tsconfig.json`. Lalu kita dapat menggunakan perintah:

```
tsc --watch
```

Atau:

```
tsc -w
```

untuk melakukan kompilasi seluruh project yang ada.
