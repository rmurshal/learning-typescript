/**
 * Mencegah error saat mencoba mengakses property yang mungkin undefined
 */
const userData = {
    name: 'Rama',
    // job: {
    //     title: 'CEO',
    //     description: 'My Company',
    // },
}
// Gunakan "?"
console.log(userData?.job?.title)

/**
 * Null Coalescing - Return kondisi pertama jika bukan null atau undefined
 * Selain itu, kondisi kedua
 * Ini berbeda dengan menggunakan ||, yang berarti nilai seperti 0 atau "" berarti falsy
 */
const userInput = null
const storedData = userInput ?? 'User Input Otomatis'
console.log(storedData)
