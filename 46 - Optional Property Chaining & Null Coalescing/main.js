var _a;
/**
 * Mencegah untuk mengakses apabila sebuah variabel bernilai null | undefined
 */
var userData = {
    name: 'Rama'
};
// Gunakan "?"
console.log((_a = userData === null || userData === void 0 ? void 0 : userData.job) === null || _a === void 0 ? void 0 : _a.title);
/**
 * Null Coalescing
 */
var userInput = null;
var storedData = userInput || 'User Input Otomatis';
console.log(storedData);
