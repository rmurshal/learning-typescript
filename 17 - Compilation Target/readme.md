# Compilation target

Pada salah satu property yang ada `compilerOptions`, terdapat property `target` yang berguna untuk menetapkan target kompilasi javascript versions.

Notes:

-   Semua browser support terhadap `es3`.
-   Semua modern browser kecuali Internet Explorer 9 tidak support terhadap `ES5 "use strict"`.
-   Semua modern browser (termasuk `Edge 14`) support terhadap `ES6 2015`.

Source javascript versions: https://www.w3schools.com/js/js_versions.asp
