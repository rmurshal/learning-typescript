class Department {
    private employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

// ok, "DTI1" and "Information Technology" will automatically passed on Department class
class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'Information Technology')
    }
}

class AccountingDepartment extends Department {
    constructor(id: string, public reports: string[]) {
        super(id, 'Accounting')
    }

    addReports(report: string) {
        this.reports.push(report)
    }
}

const DTI1 = new ITDepartment('DTI1', ['Mas Agus', 'Pak Hari'])
console.log(DTI1)

const AC1 = new AccountingDepartment('AC1', ['Reports 1'])
AC1.addReports('Reports 2')
AC1.addEmployee('Blz')
console.log(AC1)
