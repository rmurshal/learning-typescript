# Inheritance

Untuk melakukan inheritance, kita menggunakan keyword `extends` terhadap kelas yang akan kita warisi.

Terdapat beberapa kasus untuk melakukan inheritance, antara lain:

### Tidak terdapat property atau method baru yang ingin ditambahkan

```ts
class Department {
    private employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

// ok, "DTI1" and "Information Technology" will automatically passed on Department class
class ITDepartment extends Department {}

const DTI1 = new ITDepartment('DTI1', 'Information Technology 1')
```

### Akan menambahkan property baru

```ts
class Department {
    private employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        // must call "super" here...
        super(id, 'Information Technology')
    }
}

// here we are not required to pass all the "Department" property
const DTI1 = new ITDepartment('DTI1', ['Mas Agus', 'Pak Hari'])
```
