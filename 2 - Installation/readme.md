# Install Typescript

## Untuk melakukan instalasi, jalankan perintah:
```
npm i -g typescript
```

## Untuk melakukan kompilasi kode Typescript ke Javascript, jalankan perintah:
```
tsc index.tsx
```
*Notes: tsc stands for Typescript Compiler.*
