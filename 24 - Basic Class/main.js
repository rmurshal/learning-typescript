var Department = /** @class */ (function () {
    function Department(name) {
        this.name = name;
    }
    return Department;
}());
var department = new Department('Information Technology');
console.log(department);
