class Department {
    name: string

    constructor(name: string) {
        this.name = name
    }
}

let department = new Department('Information Technology')

console.log(department)
