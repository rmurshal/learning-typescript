# Basic Class

```ts
class Department {
    name: string

    constructor(name: string) {
        this.name = name
    }
}

let department = new Department('Information Technology')

console.log(department)
```

Hasil kompilasi `Class` tergantung dari target javascript environmentnya, apabila ES6+ maka keyword `Class` sudah ada. Selain itu `Class` dibuat menggunakan `function`.
