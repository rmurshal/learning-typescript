const number1 = document.getElementById("number1") as HTMLInputElement;
const number2 = document.getElementById("number2") as HTMLInputElement;
const button = document.getElementById("button") as HTMLInputElement;

const add = (val1: number, val2: number) => {
  return val1 + val2;
};

button.addEventListener("click", () => {
  console.log(add(+number1.value, +number2.value));
});
