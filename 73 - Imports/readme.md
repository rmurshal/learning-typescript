# Imports

Konsepnya kurang lebih sama dengan `exports` dan `imports` pada ES6 modules.

_Notes: Contoh mengimport file `main.ts`, kita akan menggunakan `main` atau `main.js`._
