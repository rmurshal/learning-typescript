# Function Overload

Berguna untuk menetapkan tipe return value dari sebuah function apabila terdapat beberapa kemungkinan return valuenya.

```ts
type numeric = number | string

// Declare a Function Overload
function getAdd(a: string, b: string): string
function getAdd(a: number, b: number): number
function getAdd(a: string, b: number): string
function getAdd(a: number, b: string): string

function getAdd(a: numeric, b: numeric) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString()
    }
    return a + b
}

const result = getAdd(1, 2)
const result2 = getAdd('Rama ', 'Muhammad')

// Notes: Without function overload, "result" and "result2" will be numeric type
```
