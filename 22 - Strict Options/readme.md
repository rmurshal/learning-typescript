# Strict Options

## Notes

Opsi strict yang ada:

![All Strict Options](./strict.png)

## strict

Apabila enable dan true, maka semua opsi strict (yang ada di bawahnya) akan otomatis enable.

## noImplicitAny

Berguna untuk memberi error apabila parameter pada sebuah function tidak terdapat tipe data.

_Notes: Pada sebuah variabel biasa, typescript mengizinkan untuk tidak memberi tipe data karena terdapat fitur inference-nya_

## strictNullChecks

Berguna untuk memberi error apabila kita berusaha untuk mengakses variabel yang kemungkinan dapat bernilai `null`. Contoh saat mengakses DOM. Bisa diatas dengan tricky way (menggunakan `!`) atau menggunakan if conditional.

## strictFunctionTypes

Berguna untuk mengecek apakah parameter dan return value dari sebuah function sudah benar. Kode berikut akan tidak memiliki error apabila `strictFunctionTypes` tidak dijalankan:

```ts
type MyFuncType = (a: string) => void

function myFunc(arg: any) {
    console.log(arg)
}

const myFuncType: MyFuncType = myFunc
```

## strictBindCallApply

Berguna untuk memberikan error apabila sebuah function membutuhkan parameter tetapi saat dipanggil tidak diberikan parameter yang sesuai. Contoh berikut tidak akan memberikan error saat `strictBindCallApply` bernilai `false`:

```ts
const button = document.querySelector('button')!

function clickHandler(message: string) {
    console.log('clicked')
}

button.addEventListener('click', clickHandler.bind(null))
```

_More about `bind` with example: https://levelup.gitconnected.com/bind-functions-in-javascript-f18beb160a82_

## alwaysStrict

Berguna untuk menghapus keyword `"use strict"` pada hasil kompilasi javascript.
