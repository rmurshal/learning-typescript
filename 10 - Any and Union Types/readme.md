# Any types

Tipe data any digunakan saat kita **tidak tahu** harus memberi tipe data apa pada sebuah variabel. Semaksimal mungkin untuk mencegah menggunakan tipe data ini.

```ts
let idk: any = ['1', 2, false, 'zzz']
```

# Union types

Tipe data union memungkinkan kita untuk dapat menetapkan berbagai macam tipe data pada sebuah variabel.

```ts
let numstr: number | string = 'rama'
numstr = 99 // ok
```
