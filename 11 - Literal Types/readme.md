# Literal types

Kurang lebih sama seperti union type, tapi valuenya berupa sebuah nilai (bukan tipe data).

# Contoh 1 - basic

```ts
function calc(num1: number, num2: number, opr: 'sum' | 'subs') {
    if (opr === 'sum') {
        return num1 + num2
    } else if (opr === 'subs') {
        return num1 - num2
    } else {
        return 0
    }
}
```

## Contoh 2 - menggunakan enum

```ts
enum operations {
    MULT = 'mult',
    DIV = 'div',
}

function calc2(num1: number, num2: number, opr: operations) {
    if (opr === operations.MULT) {
        return num1 * num2
    } else if (opr === operations.DIV) {
        return num1 / num2
    } else {
        return 0
    }
}
```
