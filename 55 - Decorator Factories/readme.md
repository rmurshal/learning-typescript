# Decorator Factories

Adalah sebuah fungsi yang akan mereturn sebuah fungsi decorator, tetapi kita dapat pass sebuah variabel kedalam fungsi factories ini.

```ts
function Logger(logString: string) {
    return function (constructor: Function) {
        console.log(logString)
        console.log(constructor)
    }
}

// Here, we not call like @Logger, but @Logget('Params')
@Logger('Logging for the class Person')
class Person {
    name = 'Blz'

    constructor() {
        console.log('Creating object...')
    }
}
```
