// Basic Generics
let names: Array<string> = [] // Same as string[]

// Example Usage of Generics
let promise: Promise<string> = new Promise((resolve, _reject) => {
    setTimeout(() => {
        resolve('Will Return Some String')
    }, 2000)
})
promise.then((data) => {
    // Without specify Promise<string>, typescript does not know the return type of the Promise, so we can't apply .split() function here
    console.log(data.split(''))
})
