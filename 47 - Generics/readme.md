# Generics

Generics adalah pengembangan fitur typescript yang digunakan untuk menspesifikkan tipe data dari suatu hal (misal array, component function, etc).

Berikut adalah contoh basic dari generics:

```ts
let names: Array<string> = [] // Same as string[]
```

Berikut adalah kegunaan lain dari generics:

```ts
let promise: Promise<string> = new Promise((resolve, _reject) => {
    setTimeout(() => {
        resolve('Will Return Some String')
    }, 2000)
})
promise.then((data) => {
    // Without specify Promise<string>, typescript does not know the return type of the Promise, so we can't apply .split() function here
    console.log(data.split(''))
})
```
