class Department {
    // change to protected here...
    protected employees: string[] = []

    constructor(private readonly id: string, public name: string) {}

    addEmployee(employee: string) {
        this.employees.push(employee)
    }

    printEmployees(this: Department) {
        console.log(this.employees, this.employees.length)
    }

    describe(this: Department) {
        console.log(`Department with id ${this.id} : ${this.name}`)
    }
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'Information Technology')
    }

    // override addEmployee - adding validation (example)
    addEmployee(employee: string): void {
        if (employee === 'Pak Hari') {
            return
        }
        this.employees.push(employee)
    }
}

const DTI1 = new ITDepartment('DTI1', ['Pak Agus', 'Pak Hari'])
DTI1.addEmployee('Mas Dimas')
DTI1.addEmployee('Pak Hari')
console.log(DTI1)
