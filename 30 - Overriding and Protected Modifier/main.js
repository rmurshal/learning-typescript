var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Department = /** @class */ (function () {
    function Department(id, name) {
        this.id = id;
        this.name = name;
        // change to protected here...
        this.employees = [];
    }
    Department.prototype.addEmployee = function (employee) {
        this.employees.push(employee);
    };
    Department.prototype.printEmployees = function () {
        console.log(this.employees, this.employees.length);
    };
    Department.prototype.describe = function () {
        console.log("Department with id ".concat(this.id, " : ").concat(this.name));
    };
    return Department;
}());
var ITDepartment = /** @class */ (function (_super) {
    __extends(ITDepartment, _super);
    function ITDepartment(id, admins) {
        var _this = _super.call(this, id, 'Information Technology') || this;
        _this.admins = admins;
        return _this;
    }
    // override addEmployee - adding validation (example)
    ITDepartment.prototype.addEmployee = function (employee) {
        if (employee === 'Pak Hari') {
            return;
        }
        this.employees.push(employee);
    };
    return ITDepartment;
}(Department));
var DTI1 = new ITDepartment('DTI1', ['Pak Agus', 'Pak Hari']);
DTI1.addEmployee('Mas Dimas');
DTI1.addEmployee('Pak Hari');
console.log(DTI1);
